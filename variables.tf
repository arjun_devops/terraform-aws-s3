variable "stage" {
  type        = string
  description = "Stage, e.g. 'prod', 'staging', 'dev', or 'test'"
  default     = ""
}

variable "domain" {
  type        = string
  description = "domain"
  default     = ""
}

variable "cert_arn" {
  type        = string
  description = "certificate arn in string"
  default     = ""
}

variable "name" {
  type        = string
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "delimiter" {
  type        = string
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "parent_zone_id" {
  type        = string
  default     = ""
  description = "ID of the hosted zone to contain this record  (or specify `parent_zone_name`)"
}

variable "aliases" {
  type        = list(string)
  description = "List of FQDN's - Used to set the Alternate Domain Names (CNAMEs) setting on Cloudfront"
  default     = []
}

variable "parent_zone_name" {
  type        = string
  default     = ""
  description = "Name of the hosted zone to contain this record (or specify `parent_zone_id`)"
}

variable "enabled" {
  type        = bool
  default     = true
  description = "Select Enabled if you want CloudFront to begin processing requests as soon as the distribution is created, or select Disabled if you do not want CloudFront to begin processing requests after the distribution is created."
}

variable "bucket_acl" {
  type        = string
  default     = "private"
  description = "bucket ACL"
}

variable "bucket_hosted_zone_id" {
  type        = string
  default     = "Z3O0J2DXBE1FTB"
  description = "bucket hosted_zone_id"
}

variable "region" {
  type        = string
  default     = "ap-southeast-1"
  description = "the region"
}

variable "bucket_versioning" {
  type        = bool
  default     = true
  description = "whether to enable versioning or not"
}

variable "bucket_mfa_delete" {
  type        = bool
  default     = false
  description = "whether to enable mfa delete or not"
}

variable "bucket_log_target_bucket" {
  type        = string
  default     = "s3bucketlogsglobal"
  description = "where to store the bucket logs"
}

variable "bucket_log_target_prefix" {
  type        = string
  default     = "logs/"
  description = "logs location in target bucket"
}

variable "bucket_doc_root" {
  type        = string
  default     = "index.html"
  description = "documet root of bucket"
}

variable "bucket_error_root" {
  type        = string
  default     = "error.html"
  description = "error page for bucket"
}

variable "cloudfront_enabled" {
  type        = bool
  default     = true
  description = "whether to enable cloudfront or not"
}

variable "cloudfront_ipv6" {
  type        = bool
  default     = false
  description = "whether to enable ipv6 in cloudfront or not"
}

variable "cloudfront_http_version" {
  type        = string
  default     = "http2"
  description = "cloudfront http version"
}

variable "cloudfront_doc_root" {
  type        = string
  default     = "index.html"
  description = "cloudfront document root"
}
