provider "aws" {
  alias = "ap-southeast-1"
}


locals {
  name = replace(var.domain, ".", "-")
}

###################### naming convention #####################################

module "label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.15.0"
  name        = var.name
  stage       = var.stage
  delimiter   = var.delimiter
  attributes  = var.attributes
  tags        = var.tags
  label_order = ["environment", "name", "stage", "attributes"]
}


##############################s3 bucket creation ###################################

resource "aws_s3_bucket" "default" {
  acl            = var.bucket_acl
  bucket         = local.name
  force_destroy  = "false"
  hosted_zone_id = var.bucket_hosted_zone_id
  region         = var.region
  request_payer  = "BucketOwner"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled    = var.bucket_versioning
    mfa_delete = var.bucket_mfa_delete
  }

  logging {
    target_bucket = var.bucket_log_target_bucket
    target_prefix = var.bucket_log_target_prefix
  }

  website {
    index_document = var.bucket_doc_root
    error_document = var.bucket_error_root
  }

  tags = module.label.tags
}


resource "aws_s3_bucket_policy" "default" {
  bucket = aws_s3_bucket.default.bucket

  policy = <<POLICY
{
  "Id": "PolicyForCloudFrontPrivateContent",
  "Statement": [
    {
      "Action": "s3:GetObject",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_cloudfront_origin_access_identity.default.iam_arn}"
      },
      "Resource": "${aws_s3_bucket.default.arn}/*",
      "Sid": "1"
    },
    {
      "Action": "s3:*",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::207606013102:role/pomelo-dev-ec2-role"
      },
      "Resource": [
        "${aws_s3_bucket.default.arn}/*"
      ]
    }
  ],
  "Version": "2008-10-17"
}
POLICY

}

########################### cloudfront ###################################################

resource "aws_cloudfront_origin_access_identity" "default" {
  comment = "origin access identity for ${var.domain}"
}

resource "aws_cloudfront_distribution" "default" {
  default_cache_behavior {
    allowed_methods = ["HEAD", "GET"]
    cached_methods  = ["HEAD", "GET"]
    compress        = "true"
    default_ttl     = "86400"

    forwarded_values {
      cookies {
        forward = "none"
      }

      headers      = ["Origin"]
      query_string = "false"
    }

    max_ttl                = "31536000"
    min_ttl                = "0"
    smooth_streaming       = "false"
    target_origin_id       = local.name
    viewer_protocol_policy = "redirect-to-https"
  }

  aliases = [
    var.domain,
  ]

  enabled             = var.cloudfront_enabled
  http_version        = var.cloudfront_http_version
  is_ipv6_enabled     = var.cloudfront_ipv6
  default_root_object = var.cloudfront_doc_root

  origin {
    origin_id   = local.name
    domain_name = aws_s3_bucket.default.bucket_domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path
    }
  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  retain_on_delete = "false"

  viewer_certificate {
    acm_certificate_arn            = var.cert_arn
    cloudfront_default_certificate = false
    minimum_protocol_version       = "TLSv1.2_2018"
    ssl_support_method             = "sni-only"
  }
}

##################################### dns ########################################

module "dns" {
  source          = "git::https://github.com/cloudposse/terraform-aws-route53-alias.git?ref=tags/0.4.0"
  enabled         = var.enabled && (var.parent_zone_id != "" || var.parent_zone_name != "") ? true : false
  aliases         = var.aliases
  parent_zone_id  = var.parent_zone_id
  target_dns_name = aws_cloudfront_distribution.default.domain_name
  target_zone_id  = aws_cloudfront_distribution.default.hosted_zone_id
  ipv6_enabled    = "false"
}
