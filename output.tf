output "bucket_id" {
  value = aws_s3_bucket.default.id
}

output "bucket_policy_id" {
  value = aws_s3_bucket_policy.default.id
}

output "cloudfront_id" {
  value = aws_cloudfront_distribution.default.id
}
