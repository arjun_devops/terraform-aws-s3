# Pomelo.

## terraform-aws-s3 🏗 🏢 🏬

`terraform-aws-se` is a terraform module that is used to create custom AWS Elastic Beanstalk environments.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- You have a Mac / Linux Workstation.
- You have installed the following software:
  -  Docker (`~> v19.03.5`)
  -  Terraform (`~> v0.12.18`)
- You have read all [guides](https://pomelofashion.atlassian.net/wiki/spaces/ENG/pages/809697581/Terraform) and documentation related to this project.

## Using `terraform-aws-s3`

To use `terraform-aws-s3`, follow these steps:

```hcl
provider "aws" {
  alias = var.region
}


locals {
    name = replace(var.domain,".","-")
}

###################### naming convention #####################################

module "label" {
  source      = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.15.0"
  name        = var.name
  stage       = var.stage
  delimiter   = var.delimiter
  attributes  = var.attributes
  tags        = var.tags
  label_order = ["environment", "name", "stage", "attributes"]
}


##############################s3 bucket creation ###################################

resource "aws_s3_bucket" "default" {
  acl            = var.bucket_acl
  bucket         = local.name
  force_destroy  = "false"
  hosted_zone_id = var.bucket_hosted_zone_id
  region         = var.region
  request_payer  = "BucketOwner"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled    = var.bucket_versioning
    mfa_delete = var.bucket_mfa_delete
  }

  logging {
    target_bucket = var.bucket_log_target_bucket
    target_prefix = var.bucket_log_target_prefix
  }

  website {
    index_document = var.bucket_doc_root
    error_document = var.bucket_error_root
  }

  tags = module.label.tags
}


resource "aws_s3_bucket_policy" "default" {
  bucket = aws_s3_bucket.default.bucket

  policy = <<POLICY
{
  "Id": "PolicyForCloudFrontPrivateContent",
  "Statement": [
    {
      "Action": "s3:GetObject",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_cloudfront_origin_access_identity.default.iam_arn}"
      },
      "Resource": "${aws_s3_bucket.default.arn}/*",
      "Sid": "1"
    },
    {
      "Action": "s3:*",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::207606013102:role/pomelo-dev-ec2-role"
      },
      "Resource": [
        "${aws_s3_bucket.default.arn}/*"
      ]
    }
  ],
  "Version": "2008-10-17"
}
POLICY

}

########################### cloudfront ###################################################

resource "aws_cloudfront_origin_access_identity" "default" {
  comment = "origin access identity for ${var.domain}"
}

resource "aws_cloudfront_distribution" "default" {
  default_cache_behavior {
    allowed_methods = ["HEAD", "GET"]
    cached_methods  = ["HEAD", "GET"]
    compress        = "true"
    default_ttl     = "86400"

    forwarded_values {
      cookies {
        forward = "none"
      }

      headers      = ["Origin"]
      query_string = "false"
    }

    max_ttl                = "31536000"
    min_ttl                = "0"
    smooth_streaming       = "false"
    target_origin_id       = local.name
    viewer_protocol_policy = "redirect-to-https"
  }

  aliases = [
    var.domain,
  ]

  enabled             = var.cloudfront_enabled
  http_version        = var.cloudfront_http_version
  is_ipv6_enabled     = var.cloudfront_ipv6
  default_root_object = var.cloudfront_doc_root

  origin {
    origin_id   = local.name
    domain_name = aws_s3_bucket.default.bucket_domain_name
  
  s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.default.cloudfront_access_identity_path
    }
  }

  price_class = "PriceClass_All"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  retain_on_delete = "false"

  viewer_certificate {
    acm_certificate_arn            = var.cert_arn
    cloudfront_default_certificate = false
    minimum_protocol_version       = "TLSv1.2_2018"
    ssl_support_method             = "sni-only"
  }
}

##################################### dns ########################################

module "dns" {
  source           = "git::https://github.com/cloudposse/terraform-aws-route53-alias.git?ref=tags/0.4.0"
  enabled          = var.enabled && (var.parent_zone_id != "" || var.parent_zone_name != "") ? true : false
  aliases          = var.aliases
  parent_zone_id   = var.parent_zone_id
  target_dns_name  = aws_cloudfront_distribution.default.domain_name
  target_zone_id   = aws_cloudfront_distribution.default.hosted_zone_id
  ipv6_enabled     = "false"
}
```

**NOTE:**

In order to use this module, you'll have to set up a terraform cloud token.

```bash
# ~/.terraform.rc
credentials "app.terraform.io" {
  token = "YOUR_TFC_TOKEN"
}
```

## Module documentation

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | 2.56.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| aliases | List of FQDN's - Used to set the Alternate Domain Names (CNAMEs) setting on Cloudfront | `list(string)` | `[]` | no |
| attributes | Additional attributes (e.g. `1`) | `list(string)` | `[]` | no |
| bucket\_acl | bucket ACL | `string` | `"private"` | no |
| bucket\_doc\_root | documet root of bucket | `string` | `"index.html"` | no |
| bucket\_error\_root | error page for bucket | `string` | `"error.html"` | no |
| bucket\_hosted\_zone\_id | bucket hosted\_zone\_id | `string` | `"Z3O0J2DXBE1FTB"` | no |
| bucket\_log\_target\_bucket | where to store the bucket logs | `string` | `"s3bucketlogsglobal"` | no |
| bucket\_log\_target\_prefix | logs location in target bucket | `string` | `"logs/"` | no |
| bucket\_mfa\_delete | whether to enable mfa delete or not | `bool` | `false` | no |
| bucket\_versioning | whether to enable versioning or not | `bool` | `true` | no |
| cert\_arn | certificate arn in string | `string` | `""` | no |
| cloudfront\_doc\_root | cloudfront document root | `string` | `"index.html"` | no |
| cloudfront\_enabled | whether to enable cloudfront or not | `bool` | `true` | no |
| cloudfront\_http\_version | cloudfront http version | `string` | `"http2"` | no |
| cloudfront\_ipv6 | whether to enable ipv6 in cloudfront or not | `bool` | `false` | no |
| delimiter | Delimiter to be used between `name`, `namespace`, `stage`, etc. | `string` | `"-"` | no |
| domain | domain | `string` | `""` | no |
| enabled | Select Enabled if you want CloudFront to begin processing requests as soon as the distribution is created, or select Disabled if you do not want CloudFront to begin processing requests after the distribution is created. | `bool` | `true` | no |
| name | Solution name, e.g. 'app' or 'cluster' | `string` | n/a | yes |
| parent\_zone\_id | ID of the hosted zone to contain this record  (or specify `parent_zone_name`) | `string` | `""` | no |
| parent\_zone\_name | Name of the hosted zone to contain this record (or specify `parent_zone_id`) | `string` | `""` | no |
| region | the region | `string` | `"ap-southeast-1"` | no |
| stage | Stage, e.g. 'prod', 'staging', 'dev', or 'test' | `string` | `""` | no |
| tags | Additional tags (e.g. `map('BusinessUnit`,`XYZ`) | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| bucket\_id | n/a |
| bucket\_policy\_id | n/a |
| cloudfront\_id | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Contributing to `terraform-aws-elasticbeanstalk`

To contribute to `terraform-aws-elasticbeanstalk`, follow these steps:

1. Clone this repository.
2. Read [Running code quality checks](#quality-checks).
3. Create a feature branch: `git checkout -b <branch_name>`.
4. Make your changes and commit them: `git commit -m '<commit_message>'`
5. Push to the original branch: `git push origin <branch_name>`
6. Create the pull request against `develop`.

**<a name="quality-checks"></a>Running code quality checks:**

To ensure the code quality of this project is kept consistent we make use of pre-commit hooks. To install them, run the commands below.

```bash
brew install pre-commit
pre-commit install --install-hooks -t pre-commit -t commit-msg
```

## Contact

If you want to contact someone about this project, please refer to the following channel: `#eng_squad_devops`
